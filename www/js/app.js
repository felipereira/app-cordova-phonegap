var DataPoints = [];
var DrinkList = [];
var count = 1;
var count1 = 1;

$(document).ready(function () {
    $.ajax({
        url: "http://10.40.0.170:3002/produtos-cadastrados",
        success: function (result) {
            var data = JSON.parse(result)
            for (let _i = 0; _i < data.length; _i++) {
                DataPoints.push({
                    "nome": data[_i].nome,
                    "descricao": data[_i].descricao,
                    "preco": data[_i].preco
                })
            }

            for (let _i = 0; _i < DataPoints.length; _i++) {

                $('#myUL').append(
                    '<li class="collection-item">' +
                    '<span class="title"><b> 0' + (count++) + ' Nome: ' + ' ' + DataPoints[_i].nome + '</b></span>' +
                    '<p>Descrição: ' + DataPoints[_i].descricao + '<p>' +
                    '<p>Preço: </b>R$ ' + DataPoints[_i].preco + ',00</p>' +
                    "</li>");
            }

        },
        error: function (request, error) {
            alert('Timeout request food list');
        }

    });

    $.ajax({
        url: "http://10.40.0.170:3002/bebidas-cadastradas",
        success: function (result) {
            var data = JSON.parse(result)
            for (let _i = 0; _i < data.length; _i++) {
                DrinkList.push({
                    "nome": data[_i].nome,
                    "descricao": data[_i].descricao,
                    "preco": data[_i].preco
                })
            }

            for (let _i = 0; _i < DrinkList.length; _i++) {

                $('#drink').append(
                    '<li class="collection-item">' +
                    '<span class="title"><b> 0' + (count1++) + ' Nome: ' + ' ' + DrinkList[_i].nome + '</b></span>' +
                    '<p>Descrição: ' + DrinkList[_i].descricao + '<p>' +
                    '<p>Preço: </b>R$ ' + DrinkList[_i].preco + ',00</p>' +
                    "</li>");
            }

        },
        error: function (request, error) {
            alert('Timeout request drink list');
        }

    });

});


$('.collection')
    .on('click', '.collection-item', function () {
        var nomeProduto = this.firstChild.textContent;
        Materialize.toast(nomeProduto + ' adicionado', 900);

        var $badge = $('.badge', this);
        if ($badge.length === 0) {
            $badge = $('<span class="badge brown-text">0</span>').appendTo(this);
        }

        $badge.text(parseInt($badge.text()) + 1);
    })
    .on('click', '.badge', function () {
        $(this).remove();
        return false;
    });

$(document).ready(function () {
    // the "href" attribute of the modal trigger must specify the modal ID that wants to be triggered
    $('.modal').modal();
});

$(document).ready(function () {

    $(function () {
        $.ajax({
            type: 'GET',
            url: "http://10.40.0.170:3002/produtos-cadastrados",
            success: function (response) {

                var data1 = JSON.parse(response)
                var array = data1;
                var products = {};

                for (var i = 0; i < array.length; i++) {
                    products[array[i].nome] = null;
                }
                $('input.autocomplete').autocomplete({
                    data: products,
                    limit: 20, // The max amount of results that can be shown at once. Default: Infinity.
                    onAutocomplete: function (val) {
                        // Callback function when value is autcompleted.
                    },
                    minLength: 1, // The minimum length of the input for the autocomplete to start. Default: 1.
                });
            }
        })
    })

});


$('#confirmar').on('click', function () {
    var texto = "";

    $('.badge').parent().each(function () {
        texto += this.firstChild.textContent + ': ';
        texto += this.lastChild.textContent + ', ';
    });

    $('#resumo').empty().text(texto);
});

$('.acao-limpar').on('click', function () {
    $('#numero-mesa').val('');
    $('#observacao').val('');
    $('#hamburguer').val('');
    $('#hamburguer-descricao').val('');
    $('#drink').val('');
    $('#drink-descricao').val('');
    $('.badge').remove();
});

$('.scan-qrcode').click(function () {
    cordova.plugins.barcodeScanner.scan(function (resultado) {
        if (resultado.text) {
            Materialize.toast('Mesa ' + resultado.text, 2000);
            $('#numero-mesa').val(resultado.text);
        }
    },
        function (erro) {
            Materialize.toast('Erro' + erro, 2000, 'red-text');
        });
});

$('.acao-finalizar').on('click', function () {
    $.ajax({
        url: 'http://10.40.0.170:3000/novo-pedido',
        data: {
            mesa: $('#numero-mesa').val(),
            pedido: $('#resumo').text(),
            observacao: $('#observacao').val()
        },
        error: function (erro) {
            Materialize.toast(erro.responseText, 3000, 'red-text');
        },
        success: function (dados) {
            Materialize.toast(dados, 2000);
            navigator.vibrate(500);

            $('#numero-mesa').val('');
            $('.badge').remove();
        }
    });
});

function Filter() {

    var input, filter, ul, li, a, i;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();

    ul = document.getElementById("filter");
    li = ul.getElementsByTagName("li");

    for (i = 0; i < li.length; i++) {
        a = li[i];
        if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";

        }
    }
}
setInterval(function () {
    Filter()
}, 100)

$('.tap-target').tapTarget('open');
$('.tap-target').tapTarget('close');

