var DataPoints = [];
var count = 1;

$.getJSON("http://localhost:3000/produtos-cadastrados", function (data) {
    $.each(data, function (key, value) {
        DataPoints.push(
            {
                "nome": value.nome,
                "descricao": value.descricao,
                "preco": value.preco
            }
        );
    });

    for (let _i = 0; _i < DataPoints.length; _i++) {

        $('ul').append(
            '<a class="collection-item waves-effect black-text">' + '<b>' +
            'Nome: ' + (count++) + ' ' + DataPoints[_i].nome + '</b><br><b>' +
            'Descrição: </b>' + DataPoints[_i].descricao + '<br><b>' +
            'Preço: </b>R$ ' + DataPoints[_i].preco + ',00' +
            "</a>");
    }
});

$('.collection')
    .on('click', '.collection-item', function () {
        var nomeProduto = this.firstChild.textContent;
        Materialize.toast(nomeProduto + ' adicionado', 900);

        var $badge = $('.badge', this);
        if ($badge.length === 0) {
            $badge = $('<span class="badge brown-text">0</span>').appendTo(this);
        }

        $badge.text(parseInt($badge.text()) + 1);
    })
    .on('click', '.badge', function () {
        $(this).remove();
        return false;
    });

$('.modal-trigger').leanModal();

$('#confirmar').on('click', function () {
    var texto = "";

    $('.badge').parent().each(function () {
        texto += this.firstChild.textContent + ': ';
        texto += this.lastChild.textContent + ', ';
    });

    $('#resumo').empty().text(texto);
});

$('.acao-limpar').on('click', function () {
    $('#numero-mesa').val('');
    $('#observacao').val('');
    $('#hamburguer').val('');
    $('#hamburguer-descricao').val('');
    $('#drink').val('');
    $('#drink-descricao').val('');
    $('.badge').remove();
});

$('.scan-qrcode').click(function () {
    cordova.plugins.barcodeScanner.scan(function (resultado) {
        if (resultado.text) {
            Materialize.toast('Mesa ' + resultado.text, 2000);
            $('#numero-mesa').val(resultado.text);
        }
    },
        function (erro) {
            Materialize.toast('Erro' + erro, 2000, 'red-text');
        });
});

$('.acao-finalizar').on('click', function () {
    $.ajax({
        url: 'http://192.168.1.32:3000/novo-pedido',
        data: {
            mesa: $('#numero-mesa').val(),
            pedido: $('#resumo').text(),
            observacao: $('#observacao').val()
        },
        error: function (erro) {
            Materialize.toast(erro.responseText, 3000, 'red-text');
        },
        success: function (dados) {
            Materialize.toast(dados, 2000);
            navigator.vibrate(500);

            $('#numero-mesa').val('');
            $('.badge').remove();
        }
    });
});

function Filter() {

    var input, filter, ul, li, a, i;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();

    ul = document.getElementById("myUL");
    li = ul.getElementsByTagName("a");

    for (i = 0; i < li.length; i++) {
        a = li[i];
        if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";

        }
    }
}

$('.tap-target').tapTarget('open');
$('.tap-target').tapTarget('close');
